package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"
)

const (
	corda = "Universe"
	uzl = "UZLeuven-gast"
	macosCmd = "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport"
)

func main() {
	home := os.Getenv("HOME_SSID")
	slacktoken := os.Getenv("SLACK_TOKEN")
	println(home)
	print(slacktoken)
	ticker := time.NewTicker(15 * time.Minute)
	var current string
	// Call on startup.
	if ssid, err := getMacSSID(); err == nil && ssid != current {
		current = ssid
		setStatus(current)
	}
	for {
		select {
		case <-ticker.C:
			if ssid, err := getMacSSID(); err == nil && ssid != current {
				current = ssid
				setStatus(current)
			}
		}
	}
}

func setStatus(status string) {
	home := os.Getenv("HOME_SSID")
	slacktoken := os.Getenv("SLACK_TOKEN")
	if c, err := NewClient(slacktoken); err == nil {
		log.Print("Changing status to " + status)
		switch status {
		case home:
			c.SetProfileStatus(&SlackStatus{
				StatusEmoji: ":house_with_garden:",
			})
		case corda:
			c.SetProfileStatus(&SlackStatus{
				StatusEmoji: ":corda-campus:",
			})
		case uzl:
			c.SetProfileStatus(&SlackStatus{
				StatusEmoji: ":uzl:",
			})
		default:
			c.SetProfileStatus(&SlackStatus{
				StatusEmoji: ":coffee:",
			})
		}
	}
}

func getCmdOutput(program string, args ...string) (string, error) {
	output, err := exec.Command(program, args...).Output()
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(string(output)), nil
}

func getMacSSID() (string, error) {
	output, err := getCmdOutput(macosCmd, "-I")
	if err != nil {
		return "", err
	}

	r := regexp.MustCompile(`s*SSID: (.+)s*`)
	name := r.FindAllStringSubmatch(output, -1)

	if len(name) <= 1 {
		return "", fmt.Errorf("Could not get SSID")
	} else {
		return name[1][1], nil
	}
}
